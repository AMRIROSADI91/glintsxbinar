function caltCubeVolume(edge) {
  return edge ** 3;
}

let cube = caltCubeVolume(32);

function caltBeamVolume(length, breadth, thickness) {
  return length * breadth * thickness;
}

let beam = caltBeamVolume(18, 2, 4);

console.log(cube + beam);
