const index = require("../index"); // Import index to run rl on this file

// Function to calculate cube volume
function calculateVolumeCone(radius, height) {
  return Math.round((1 / 3) * Math.PI * radius ** 2 * height);
}

/* Way 1 */
// Function for inputing radius of cone
function inputRadius() {
  index.rl.question(`Radius: `, (radius) => {
    if (radius > 0 && !index.isEmptyOrSpaces(radius)) {
      inputHeight(radius);
    } else {
      console.log(`Radius must be a number\n`);
      inputRadius();
    }
  });
}

// Function for inputing height of beam
function inputHeight(radius) {
  index.rl.question(`Height: `, (height) => {
    if (height > 0 && !index.isEmptyOrSpaces(height)) {
      console.log(`\ncone: ${calculateVolumeCone(radius, height)}\n`);
      index.rl.close();
    } else {
      console.log(`Height must be a number\n`);
      inputHeight(radius);
    }
  });
}

/* End Way 1 */ // Export the input, so the another file can run this code
module.exports = { inputRadius };
