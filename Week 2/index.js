// Import readline
const readline = require("readline");
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

const cuboid = require("./function/cuboid"); // import cuboid
const cube = require("./function/cube"); // import cube
const cone = require("./function/cone"); // import cone
// // const tube = require("./function/kerucut"); // import tube
// const sphere = require("./function/sphere"); // import sphere

function isEmptyOrSpaces(str) {
  return str === null || str.match(/^ *$/) !== null;
}

// Function to display the menu
function menu() {
  console.log(`Menu`);
  console.log(`====`);
  console.log(`
  1. Cuboid
  2. Cube
  3. Cone
  4. Tube
  5. Sphere
  6. Exit
  `);

  rl.question(`Choose option: `, (option) => {
    if (option < 6 && !isEmptyOrSpaces(option)) {
      // If option is a number it will go here
      if (option == 1) {
        cuboid.inputLength(); // It will call input() function in cuboid file
      } else if (option == 2) {
        cube.input(); // It will call input() function in cube file
      } else if (option == 3) {
        cone.inputRadius(); // It will call inputRadius() function in cone file
      } else if (option == 4) {
        tube.inputRadius(); // It will call inputRadius() function in kerucut file
      } else if (option == 5) {
        sphere.inputRadius(); // It will call inputRadius() function in kerucut file
      } else if (option == 6) {
        rl.close(); // It will close the program
      } else {
        console.log(`Option must be 1 to 6!\n`);
        menu(); // If option is not 1 to 6, it will go back to the menu again
      }
    }
  });
}

menu(); // call the menu function to display the menu

module.exports.rl = rl; // export rl to make another can run the readline
module.exports.isEmptyOrSpaces = isEmptyOrSpaces;
