/* If we use callback function, we can't add another logic outside the function */

// Import readline
const readline = require("readline");
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

let edgeg = 0;
let lengthg = 0;
let breadthg = 0;
let thicknessg = 0;

// Function to calculate cube volume
function cube(edge) {
  return edge ** 3;
}

// Function for inputing length of cube
function inputEdge() {
  rl.question(`Edge: `, (edge) => {
    if (!isNaN(edge) && edge > 0) {
      console.log(`\nVolume Cube: ${cube(edge, edge, edge)}`);
      let edged = edgeg + edge;
      console.log(`\nBeam`);
      console.log(`=========`);
      inputLength(); // Call way 1();
    } else {
      console.log(`edge must be a number\n`);
      inputEdge();
    }
  });
}

// Function to calculate beam volume
function beam(length, breadth, thickness) {
  return length * breadth * thickness;
}

/* Way 1 */
// Function for inputing length of beam
function inputLength() {
  rl.question(`Length: `, (length) => {
    if (!isNaN(length) && length > 0) {
      inputBreadth(length);
    } else {
      console.log(`Length must be a number\n`);
      inputLength();
    }
  });
}

// Function for inputing breadth of beam
function inputBreadth(length) {
  rl.question(`Breadth: `, (breadth) => {
    if (!isNaN(breadth) && breadth > 0) {
      inputThickness(length, breadth);
    } else {
      console.log(`Breadth must be a number\n`);
      inputBreadth(length);
    }
  });
}

// Function for inputing thickness of beam
function inputThickness(length, breadth) {
  rl.question(`Thickness: `, (thickness) => {
    if (!isNaN(thickness) && thickness > 0) {
      console.log(`\nVolume Beam: ${beam(length, breadth, thickness)}`);
      // let lengthg = lengthg + length;
      // let breadthg = breadthg + breadth;
      // let thicknessg = thicknessg + thickness;
      totalCubeBeam(cube, beam);

      // rl.close();
    } else {
      console.log(`Thickness must be a number\n`);
      inputThickness(length, width);
    }
  });
}

function totalCubeBeam(cube, beam) {
  console.log("\nTotal Volume cube+beam");
  let cube1 = edgeg ** 3;
  let beam1 = lengthg * breadthg * thicknessg;
  const sum = cube1 + beam1;
  console.log(sum);
  rl.close();
}

/* End Way 1 */

// /* Alternative Way */
// // All input just in one code
// function input() {
//   rl.question("Length: ", function (length) {
//     length++;
//     console.log(length);
//     rl.question("Width: ", (width) => {
//       rl.question("Height: ", (height) => {
//         if (!isNaN(length) && !isNaN(width) && !isNaN(height)) {
//           console.log(`\nBeam: ${beam(length, width, height)}`);
//           rl.close();
//         } else {
//           console.log(`Length, Width and Height must be a number\n`);
//           input();
//         }
//       });
//     });
//   });
// }
/* End Alternative Way */

console.log(`Cube`);
console.log(`=========`);
inputEdge(); // Call way 1

// input(); // Call Alternative Way
