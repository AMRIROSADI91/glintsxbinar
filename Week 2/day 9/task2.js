const login = require("../day 10/login");

const patient = [
  { name: "abu lahab", status: "positive" },
  { name: "abu jahal", status: "positive" },
  { name: "abu sofyan", status: "suspect" },
  { name: "abu tholib", status: "suspect" },
  { name: "abu bakar", status: "negative" },
  { name: "umar", status: "negative" },
];

const posPatient = patient.filter(function (ele) {
  return ele.status == "positive";
});
const susPatient = patient.filter(function (ele) {
  return ele.status == "suspect";
});
const negPatient = patient.filter(function (ele) {
  return ele.status == "negative";
});

function status() {
  console.log(``);
  console.log(`This is the secret patient Status, restricted access`);
  console.log(`====================================================`);
  console.log(`
  1. Positif
  2. Suspect
  3. Negative
  4. exit
  `);

  login.rl.question("Choose to see: ", (patien) => {
    switch (eval(patien)) {
      case 1:
        console.log(posPatient);
        login.rl.close();
        break;
      case 2:
        console.log(susPatient);
        login.rl.close();
        break;
      case 3:
        console.log(negPatient);
        login.rl.close();
        break;
      default:
        console.log("Thanks to check, take care of your self");
        login.rl.close();
        break;
    }
  });
}

status();

module.exports = { status };
