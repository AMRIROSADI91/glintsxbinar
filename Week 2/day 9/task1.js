const fruit = ["tomato", "broccoli", "kale", "cabbage", "apple"];

//way 1
// let mapFruit = fruit.filter((item) => item !== "apple"); // to filter apple
// console.log(
//   mapFruit + " " + "is a healthy food, it's definitely worth to eat."
// );

// let mapApple = fruit.filter((item) => item === "apple"); // to print apple only
// console.log(mapApple + " " + "is not a Vegetable");

//way 2
let mapFruit = fruit.map((item) => item).filter((item) => item !== "apple"); // to filter apple
console.log(
  mapFruit + " " + "is a healthy food, it's definitely worth to eat."
);

let mapApple = fruit.filter((item) => item === "apple"); // to print apple only
console.log(mapApple + " " + "is not a Vegetable");
