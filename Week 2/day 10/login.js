const EventEmitter = require("events");
const rl = require("readline").createInterface({
  input: process.stdin,
  output: process.stdout,
});

function isEmptyOrSpaces(str) {
  return str === null || str.match(/^ *$/) !== null;
}

const my = new EventEmitter();

my.on("login:failed", function (username) {
  console.log(`${username} is failed to login, try again`);
  rl.question("Username: ", (username) => {
    rl.question("Password: ", (password) => {
      login(username, password);
    });
  });
});

my.on("login:success", function (username) {
  console.log(`${username} is success to login`);
  const covid = require("../day 9/task2");
});

function login(username, password) {
  const passwordInDatabase = "123456";

  if (password !== passwordInDatabase) {
    my.emit("login:failed", username);
  } else {
    my.emit("login:success", username);
  }
}

rl.question("Username: ", (username) => {
  rl.question("Password: ", (password) => {
    login(username, password);
  });
});

module.exports.rl = rl; // export rl to make another can run the readline
module.exports.isEmptyOrSpaces = isEmptyOrSpaces;
