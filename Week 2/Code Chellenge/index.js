const data = require("./lib/arrayFactory.js");
const test = require("./lib/test.js");

/*
 * Code Here!
 * */
// Optional
function clean(data) {
  return data.filter((i) => typeof i === "number");
}

// Should return array
function sortAscending(data) {
  // Code Here
  //looping index
  for (let i = 0; i < data.length - 1; i++) {
    //looping data comparation
    for (let j = 0; j < data.length - 1; j++) {
      //algorithm bubble sort
      if (data[j] > data[j + 1]) {
        temp = data[j];
        data[j] = data[j + 1];
        data[j + 1] = temp;
      }
    }
  }
  return data.filter((i) => typeof i === "number");
}

// Should return array
function sortDecending(data) {
  // Code Here
  //looping index
  for (let i = 0; i < data.length - 1; i++) {
    //looping data comparation
    for (let j = 0; j < data.length - 1; j++) {
      //algoritm bubble sort
      if (data[j] < data[j + 1]) {
        temp = data[j];
        data[j] = data[j + 1];
        data[j + 1] = temp;
      }
    }
  }
  return data.filter((i) => typeof i === "number");
}

// DON'T CHANGE
test(sortAscending, sortDecending, data);
