const Animals = require("./Animals");
const Predator = require("./Predator");
const prey = require("./Prey");

// Make object of Person class
let lion = new Predator("Lion");
console.log(lion.fiesting());

let deer = new prey("Deer");
console.log(deer);

// To access static property
// console.log(rangga.isLife); // undefined
console.log(Animals.isSustainable); // true

// To access static method/function
console.log(Animals.address());
// console.log(adib.breath()); // Error
