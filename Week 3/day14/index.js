const express = require("express"); //

const app = express(); //

// Enable req.body
app.use(express.json());
app.use(
  express.urlencoded({
    extended: true,
  })
);

// Import routes
const jannahRoutes = require("./routes/jannahRoutes");

app.use("/", jannahRoutes);

app.listen(4000, () => console.log("Server running on port 4000"));
