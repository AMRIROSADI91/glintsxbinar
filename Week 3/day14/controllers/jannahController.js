const { request } = require("express");
const sahabahs = require("../models/sahabahs.json");

class jannahController {
  getAllSahabahs(req, res) {
    try {
      res.status(200).json({
        data: sahabahs,
      });
    } catch (error) {
      res.status(500).json({
        message: error.message,
      });
    }
  }
  getOneSahabah(req, res) {
    try {
      const sahabah = sahabahs.filter(
        (sahabah) => sahabah.id === eval(req.params.id)
      );

      res.status(200).json({
        data: sahabah,
      });
    } catch (error) {
      res.status(500).json({
        message: error.message,
      });
    }
  }

  createSahabah(req, res) {
    try {
      sahabahs.push(req.body);

      res.status(201).json({
        data: req.body,
      });
    } catch (error) {
      res.status(500).json({
        message: error.message,
      });
    }
  }
  updateSahabah(req, res) {
    try {
      const id = req.params.id;
      sahabahs.filter((sahabah) => {
        if (sahabah.id == id) {
          sahabah.id = id;
          sahabah.name = req.body.name;

          return sahabah;
        }
      });
      res.json(sahabahs);
    } catch (error) {
      res.status(500).json({
        message: error.message,
      });
    }
  }
  deleteSahabah(req, res) {
    try {
      const { id: sahabahId } = req.params;
      const indexOfSahabah = sahabahs.findIndex(
        (sahabah) => sahabah.id === +sahabahId
      );
      sahabahs.splice(indexOfSahabah, 1);
      res.send({ message: "Sahabah has been deleted" });
    } catch (error) {
      if (indexOfSahabah === -1) {
        return res.status(500).send({ message: "Sahabah not found!" });
      }
    }
  }
}

module.exports = new jannahController();
