const express = require("express");

const router = express.Router();

// import controller

const {
  getAllSahabahs,
  getOneSahabah,
  createSahabah,
  updateSahabah,
  deleteSahabah,
} = require("../controllers/jannahController");
// if user access http://localhost:4000/ or http://localhost:4000 (GET), it will go here
// to get data
router.get("/", getAllSahabahs);
router.get("/:id", getOneSahabah);

// if user access http://localhost:4000/ or http://localhost:4000 (POST), it will go here
//to send data
router.post("/", createSahabah);

// if user access http://localhost:4000/ or http://localhost:4000 (POST), it will go here
// to change data
router.put("/:id", updateSahabah);

// to delete data
router.delete("/:id", deleteSahabah);

module.exports = router;
