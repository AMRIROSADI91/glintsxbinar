// Make class Animals
class Animals {
  // Static property
  static isSustainable = true;

  // Instance property
  constructor(type) {
    this.type = type;
  }

  // Instance function
  fiesting() {
    return `${this.hunting()} then ${this.name} is fiesting!`;
  }

  // Static function
  static wild() {
    return `Animal is wild`;
  }

  hunting() {
    return `${this.name} is Hunting!`;
  }
}

// Instance method
Animals.prototype.greet = function () {
  return `Hi, ${this.name}!`;
};

// Static method
Animals.address = function () {
  return `I live in jungle!`;
};

module.exports = Animals;
