const Animals = require("./Animals");

class Prey extends Animals {
  constructor(name) {
    super("Prey");
    this.name = name;
  }
}

module.exports = Prey;
