const Animals = require("./Animals");

class Predator extends Animals {
  constructor(name) {
    super("Predator");
    this.name = name;
  }
}

module.exports = Predator;
